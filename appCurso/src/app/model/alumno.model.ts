export class Alumno {
  public id: number;
  public name: string;
  public surname: string;
  public city: string;

  constructor(id: number, name: string, surname: string, city: string) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.city = city;
  }
}
