import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ejpropertybindig',
  templateUrl: './ejpropertybindig.component.html',
  styleUrls: ['./ejpropertybindig.component.css']
})
export class EjpropertybindigComponent implements OnInit {

  texto = 'Escribe algo ...';

  constructor() {
    setTimeout(() => {
      this.texto = 'por favor';
    }, 3000);
  }

  ngOnInit() {
  }

}
